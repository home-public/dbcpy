import tkinter as tk
from tkinter import OptionMenu, StringVar
import books
import fileService
#import asyncio

class bookTab:
    def __init__(self, master1):
        self.panel1 = tk.Frame(master1)
        self.panel1.grid()
        self.options = ["None"]
        self.clicked = StringVar()
        self.drop = OptionMenu(self.panel1, self.clicked, *self.options)
        self.clicked.set(self.options[0])
        self.selected_book = None
        self.labels = []
        self.entrys = []
        #TODO: Get these from the book class. Also pages is a terrible name for this list
        self.pages = [
                { "id":"identifier", "type":"text", "contents":-1 },
                { "id":"title", "type":"input", "contents":"" },
                { "id":"images", "type":"list", "contents":["None"] },
                { "id":"cover", "type":"input", "contents":"" },
                { "id":"file", "type":"input", "contents":"" }, # TODO: This not a good name. It's the full file path
                { "id":"fileName", "type":"input", "contents":"" }, # TODO: Path to the executable. Not the best name either
                { "id":"covers", "type":"list", "contents":["None"] },
                { "id":"maker", "type":"input", "contents":"" },
                { "id":"adult", "type":"bool", "contents":False }
        ]

        #Update a book.
        def updateBook():
            if (self.selected_book is None):
                print(f"No book selected")
            saved_book = next((book for book in books.Books if book.identifier == self.selected_book.identifier), None)
            if saved_book:
                # TODO: Actually update the Book object
                saved_book.maker = self.entrys[7].get()
                print(f"Save book {saved_book.title}")
            else:
                print(f"Failed to save. Book not found")
            # TODO: This saves ALL books not just the one
            fileService.saveData(books.Books)
            print("On Save Book")

        def loadBooksFromFolders():
            #TODO: Bug: The button is not currently disabled while pressed
            self.buttonLoad.configure(state = 'disabled')
            print("Load Books From Folders")
            #asyncio.run(loadBooksAsync())
            books.loadFromFolders(callbackFunc)

        def loadBooksFromData():
            #TODO: Bug: The button is not currently disabled while pressed
            self.buttonLoadData.configure(state = 'disabled')
            print("Load Books From Data")
            #asyncio.run(loadBooksAsync())
            books.loadFromData(callbackFunc)
            
        async def loadBooksAsync():
            print("Load Books Async")
            await books.loadFromFolders(callbackFunc)

        def callbackFunc():
            print(f'Book Tab Loaded {len(books.Books)} books')
            self.buttonLoad.configure(state = 'active')
            self.buttonLoadData.configure(state = 'active')
            title_list = [book.title for book in books.Books]
            setOptions(title_list)

        def setOptions(options):
            newOptions = self.options + options
            # Reconfigure the OptionMenu with the updated options
            self.drop["menu"].delete(0, "end")  # Clear existing options
            for title in newOptions:
                self.drop["menu"].add_command(label=title, command=tk._setit(self.clicked, title))

        def on_option_selected(*args):
            selected_title = self.clicked.get()
            self.selected_book = next((book for book in books.Books if book.title == selected_title), None)
            if self.selected_book:
                handle_book_selection(self.selected_book)
            else:
                print(f"Book not found: {selected_title}")
                clearEntrys()

        #TODO: hardcoding the index is a poor solution. Also where is fileList?
        def handle_book_selection(book):
            print(f"Selected book: {book.title}")
            self.pages[0]['contents'] = book.identifier
            self.pages[1]['contents'] = book.title
            self.pages[2]['contents'] = book.images
            self.pages[3]['contents'] = book.cover
            self.pages[4]['contents'] = book.file
            self.pages[5]['contents'] = book.fileName
            self.pages[6]['contents'] = book.covers
            self.pages[7]['contents'] = book.maker
            self.pages[8]['contents'] = book.adult
            updateEntrys()

        def updateEntrys():
            for i in range(len(self.pages)):
                if (self.pages[i]["type"] == "text"): 
                    self.entrys[i].configure(text=self.pages[i]['contents'])
                elif (self.pages[i]["type"] == "list"):
                    #TODO
                    print(f"Update list entry : {len(self.pages[i]['contents'])}")
                elif (self.pages[i]["type"] == "bool"):
                    #TODO
                    print(f"Update bool entry : {self.pages[i]['contents']}")
                else:
                    self.entrys[i].delete(0, tk.END)
                    self.entrys[i].insert(0, self.pages[i]['contents'])

        def clearEntrys():
            for i in range(len(self.pages)):
                if (self.pages[i]["type"] == "text"):
                    if (self.pages[i]["id"] == "identifier"):
                        self.pages[i]['contents'] = -1
                    else:
                        self.pages[i]['contents'] = ""
                elif (self.pages[i]["type"] == "list"):
                    self.pages[i]['contents'] = []
                elif (self.pages[i]["type"] == "bool"):
                    self.pages[i]['contents'] = False
                else:
                    self.pages[i]['contents'] = ""
            updateEntrys()

        # TODO: A function to just undo whatever edits you made so you don't have to change titles back and forth
        def resetEntrys(book):
            print(f"Selected book: {book.title}")

        #Add on_option_selected after the function or it won't be defined yet
        self.clicked.trace_add("write", on_option_selected)

        #Add editable fields
        for item in self.pages:
            self.labels.append(tk.Label(self.panel1, text=item["id"], width=22, anchor=tk.W))
            if (item["type"] == "text"):
                self.entrys.append(tk.Label(self.panel1, text=item["contents"], width=22, anchor=tk.W))
            elif (item["type"] == "list"):
                options = item["contents"]
                clicked = StringVar()
                self.entrys.append(OptionMenu(self.panel1, clicked, *options))
            elif (item["type"] == "bool"):
                self.entrys.append(tk.Checkbutton(self.panel1, variable=item["contents"]))
            else:
                self.entrys.append(tk.Entry(self.panel1))
                
        
        #Add Save button
        #TODO: A bug lets you push the button during load, queueing another load
        self.buttonAdd = tk.Button(self.panel1, text="SAVE DATA", command=updateBook)
        self.buttonLoadData = tk.Button(self.panel1, text="LOAD DATA", command=loadBooksFromData)
        self.buttonLoad = tk.Button(self.panel1, text="LOAD FROM FOLDER", command=loadBooksFromFolders)

        #Place in the grid
        row = 0
        for i in range(len(self.pages)):
            self.labels[i].grid(row=row, column=0, sticky=tk.W)
            self.entrys[i].grid(row=row, column=1, sticky=tk.W)
            row=i+1

        self.buttonAdd.grid(row=row, column=0)
        self.buttonLoad.grid(row=row, column=1)
        self.buttonLoadData.grid(row=row, column=2)
        self.drop.grid(row=row, column=3)

        