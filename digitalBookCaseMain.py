import pygame
import os
from sys import exit
import books
import portals
import fileService
from settings import *

os.chdir(os.path.dirname(os.path.abspath(__file__)))

pygame.init()

# joysticks
pygame.joystick.init()
joysticks = [pygame.joystick.Joystick(x) for x in range(pygame.joystick.get_count())]
print('Found joypads : {joysticks}')

books.init()
screen = pygame.display.set_mode((WIDTH,HEIGHT))
pygame.display.set_caption(TITLE)
clock = pygame.time.Clock()

bgm = fileService.load_sound()
portal_group = portals.load_portals()

bgSurfaces = []
fgSurfaces = []

main_bg_surface = pygame.Surface((WIDTH,HEIGHT))
main_bg_surface.fill(BG_COLOR)
main_bg_rect = main_bg_surface.get_rect(topleft = (0,0))

bgSurfaces.append( {'surface': main_bg_surface, 'rect': main_bg_rect} )

# bg_surface = fileService.load_image('graphics/DBCBG.png', False)
# bg_surface = fileService.fitImage(WIDHTH,HEIGHT,bg_surface)
# bg_rect = bg_surface.get_rect(topleft = (0,0))

# bgSurfaces.append( {'surface': bg_surface, 'rect': bg_rect } )

fg_top_surface = fileService.load_image('graphics/DBCTOPBG.png', False)
fg_top_surface = fileService.fitImage(WIDTH,HEIGHT,fg_top_surface)
fg_top_mask = pygame.mask.from_surface (fg_top_surface)
fg_top_rect = fg_top_surface.get_rect(topleft = (0,-20))

fgSurfaces.append( {'surface': fg_top_surface, 'rect': fg_top_rect } )

fg_lwr_surface = fileService.load_image('graphics/DBCBOTBG.png', False)
fg_lwr_surface = fileService.fitImage(WIDTH,HEIGHT,fg_lwr_surface)
fg_lwr_mask = pygame.mask.from_surface (fg_lwr_surface)
fg_lwr_rect = fg_lwr_surface.get_rect(topleft = (0,800)) #TODO: Find out why bottomleft didn't work when topleft does

fgSurfaces.append( {'surface': fg_lwr_surface, 'rect': fg_lwr_rect } )

# TODO: Font size needs to scale with screen size
pixel_font = pygame.font.Font('font/Pixeltype.ttf', 86)
arial_font = pygame.font.SysFont('arial', 68)
ipag_font = pygame.font.Font('font/ipag.ttf', 68)
cover_drop_font = pygame.font.SysFont('font/ipag.ttf', 30)

selected_font = ipag_font

title_text = portals.ActiveTitle()
title_surface = selected_font.render(title_text, False, 'Black')
title_surface = fileService.fit_text_to_width_from_surface(title_surface, (WIDTH, title_surface.get_size()[1]))
title_rect = title_surface.get_rect(midbottom = (WIDTH/2,HEIGHT/8))
title_text_index = len(fgSurfaces)
fgSurfaces.append( {'surface': title_surface, 'rect': title_rect})

executable_text = portals.ActiveLauncher()
executable_surface = selected_font.render(executable_text, False, 'Black')
executable_surface = fileService.fit_text_to_width_from_surface(executable_surface, (WIDTH, executable_surface.get_size()[1]))
executable_rect = executable_surface.get_rect(midbottom = (WIDTH/2,HEIGHT - HEIGHT/8))
executable_text_index = len(fgSurfaces)
fgSurfaces.append( {'surface': executable_surface, 'rect': executable_rect})

# Currently default options are required.
#available_covers = ["option 1", "2nd option", "another option"]
#covers_dropdown = dropDownUI.OptionBox(140, 840, 160, 40, cover_drop_font, available_covers)

def move(_portals, dir):
    nextIndex = portals.ActiveIndex - dir
    if is_moving(_portals):
        return False
    # SetActiveIndex on Move if Horizontal
    if HORIZONTALUI:
        if nextIndex < 0 and dir == 1:
            portals.ActiveIndex = len(portal_group)-1
        elif nextIndex > len(portal_group)-1 and dir == -1:
            portals.ActiveIndex = 0
        else:
            portals.ActiveIndex = nextIndex
    for sprite in _portals:
        sprite.next(dir)
    return True

def is_moving(portals):
    for sprite in portals:
        if sprite.is_moving:
            return True
    return False

def isHovering(_portals):
    if (not isOverForeground()):
        for sprite in _portals:
            if sprite.onHover():
                if sprite.isHovered:
                    # TODO: During HORIZONTALUI Cursor is set every frame. Fix. Also title and exe text should update on mouse over if we are keeping it for Horizontal
                    if HORIZONTALUI:
                        pygame.mouse.set_cursor(pygame.SYSTEM_CURSOR_HAND)
                    elif portals.ActiveIndex != sprite.identifier:
                        # TODO: Bug: The active index will remain if you leave the portal with the mouse. This causes the cursor to not update on reentry of the same portal
                        portals.ActiveIndex = sprite.identifier
                        pygame.mouse.set_cursor(pygame.SYSTEM_CURSOR_HAND)
                        updateUI()
                return True
    pygame.mouse.set_cursor(pygame.SYSTEM_CURSOR_ARROW)
    return False

def update_title_text():
    text = portals.ActiveTitle()
    text_surface = selected_font.render(text, False, 'Black')
    fgSurfaces[title_text_index]['surface'] = fileService.fit_text_to_width_from_surface(text_surface, (WIDTH,text_surface.get_size()[1]))
    fgSurfaces[title_text_index]['rect'] = fgSurfaces[title_text_index]['surface'].get_rect(midbottom = (WIDTH/2,HEIGHT/8))

def update_exe_text():
    text = portals.ActiveLauncher()
    text_surface = selected_font.render(text, False, 'Black')
    fgSurfaces[executable_text_index]['surface'] = fileService.fit_text_to_width_from_surface(text_surface, (WIDTH,text_surface.get_size()[1]))
    fgSurfaces[executable_text_index]['rect'] = fgSurfaces[executable_text_index]['surface'].get_rect(midbottom = (WIDTH/2,HEIGHT - HEIGHT/8))

def updateUI():
    update_title_text()
    update_exe_text()
    # options = portals.ActiveAvailableCovers()
    # covers_dropdown.setOptions(options)

def isOverForeground():
    #print(f'isOverForeground check')
    # Get the mouse position
    mouse_pos = pygame.mouse.get_pos ()
    # Check if the mouse is over the rect of the foreground image
    if fg_top_rect.collidepoint (mouse_pos):
        # Get the offset of the mouse position from the top-left corner of the image
        offset_x = mouse_pos [0] - fg_top_rect.x
        offset_y = mouse_pos [1] - fg_top_rect.y
        # Get the value of the mask at the offset
        value = fg_top_mask.get_at ((offset_x, offset_y))
        # If the value is 0, it means the pixel is transparent, so we can proceed with the click event
        #print(f'the mask value is {value}')
        return value
    if fg_lwr_rect.collidepoint (mouse_pos):
        offset_x = mouse_pos [0] - fg_lwr_rect.x
        offset_y = mouse_pos [1] - fg_lwr_rect.y
        value = fg_lwr_mask.get_at ((offset_x, offset_y))
        return value
    # Otherwise, if the mouse is not over the rect of the foreground image, we can also proceed with the click event
    else:
        #print(f'the mouse is not over the rect of the foreground image')
        return False

#saving and loading bools are here because the features are tested where the pressed keys repeat
saving = False
loading = False
def callbackFunc():
    print(f'Loaded {len(books.Books)} books')
    
pygame.key.set_repeat(1, 10)
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                pygame.quit()
                exit()

            if event.key == pygame.K_s:
                if not saving:
                    for book in books.Books:
                        print(f'save book : {book.identifier}. images : {len(book.images)}')
                    fileService.saveData(books.Books)
                    saving = True

            if event.key == pygame.K_l:
                if not loading:
                    books.loadData()
                    loading = True

            if event.key == pygame.K_f:
                if not loading:
                    books.loadFromFolders(callbackFunc)
                    loading = True
        
            if event.key == pygame.K_LEFT:
                if move(portal_group.sprites(), 1):
                    if HORIZONTALUI:
                        updateUI()

            if event.key == pygame.K_RIGHT:
                if move(portal_group.sprites(), -1):
                    if HORIZONTALUI:
                        updateUI()

            # TODO: ENTER key is also on repeating. It shouldn't be or at least a launch stop is needed
            # if event.key == pygame.K_RETURN:
            #     portals.ActiveLaunch()

        if event.type == pygame.MOUSEMOTION:
            for sprite in portal_group.sprites():
                isHovering(portal_group.sprites())

        if event.type == pygame.MOUSEBUTTONDOWN:
            # selected_option = covers_dropdown.onClick()
            # if (selected_option != None):
            #     print(f'selected_option : {selected_option}')
            #     portals.SetCover(selected_option)
            if (not isOverForeground()):
                for sprite in portal_group.sprites():
                    sprite.onClick()

        if event.type == pygame.JOYBUTTONDOWN:
            # TODO: Joystick was made with a single PS4 controller. These buttons and event the joystick index could change
            print(event)

            if pygame.joystick.Joystick(0).get_button(13): # LEFT
                if move(portal_group.sprites(), 1):
                    if HORIZONTALUI:
                        updateUI()

            if pygame.joystick.Joystick(0).get_button(14): # RIGHT
                if move(portal_group.sprites(), -1):
                    if HORIZONTALUI:
                        updateUI()

            if pygame.joystick.Joystick(0).get_button(1) or pygame.joystick.Joystick(0).get_button(6): # CIRCLE or START
                portals.ActiveLaunch()

    #Draw
    for surface in bgSurfaces:
        screen.blit(surface['surface'],surface['rect'])
    portal_group.draw(screen)
    for surface in fgSurfaces:
        screen.blit(surface['surface'],surface['rect'])

    # #TODO: covers_dropdown is only needed for setting the cover image. Don't draw it unless some sort of edit mode is on
    # covers_dropdown.draw()
    # #TODO: executable_dropdown urgently needed
    # #Update
    # covers_dropdown.update()

    portal_group.update()
    pygame.display.update()
    clock.tick(FPS)