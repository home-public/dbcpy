import os
import fileService
from settings import *
import copy

class Book(object):
    def __init__(self, identifier, title, images, file, fileName, cover, fileList, maker, adult):
        super().__init__()
        self.identifier = identifier
        self.title = title
        self.images = images
        self.cover = cover

        self.maker = maker
        self.adult = adult

        if not fileName:
            fileName = 'ERROR NO FILE'
        
        if TEST:
            #TODO: Dwarf Fortress contains 21 executables. You need a scrollable dropdown
            print(f'Files in fileList of {self.identifier} : {len(fileList)}')
            #TODO: Test if fileList value is the file path (as it should be) Then replace the launcher ie the , file, fileName, bit with the first (or last? See the DF problem) value in fileList
            test = "ERROR NO FILE" if len(fileList) <= 0 else f'{list(fileList.keys())[0]}'
            print(f'fileName {fileName} : From list {test}')

        # Sets the Cover to the first available image or the first available image named cover or logo
        if images and not cover:
            index = 0
            for image in images:
                if (self.cover):
                    break
                coverFileName = os.path.basename(image)
                coverFileNameSansExtension = coverFileName.split('.')[0]
                if 'logo' in coverFileNameSansExtension or 'cover' in coverFileNameSansExtension:
                    index = images.index(image)
            self.cover = images[index]
            #print(f'Set Cover of {self.identifier} : {self.cover}')

        self.file = file
        self.fileName = fileName

        #TODO: This rather redundantly gets all possible covers
        self.covers = {}
        # self.covers.append(os.path.splitext(os.path.basename(self.cover))[0])
        self.covers[os.path.splitext(os.path.basename(self.cover))[0]] = self.cover
        for image in images:
            imageName = os.path.splitext(os.path.basename(image))[0]
            if any(x in imageName for x in ['logo', 'cover', 'main']):
                self.covers[imageName] = image

    #TODO: setCover is not updating the current portal. It has to be reloaded (scrolled off screen)
    def setCover(self, name):
        print(f'setCover: {self.covers[name]}')
        self.cover = self.covers[name]

    def Launch(self):
        if self.file:
            print(f'Launch: {self.file}')
            result = os.startfile(self.file)
            # result = subprocess.check_call(self.file, stdin=None, stdout=None, stderr=None, shell=False) # Will not Launch lnk
            print(f'result: {result}')
        else:
            print(f'No File at : {self.file}')

Books = []

Launcher = {
    'launchPath': "",
    'launcherName': ""
}

FileList = {}
AllImages = []

def getNextBook(nextIndex):
    if nextIndex < 0:
        return len(Books)+nextIndex #-1
    elif nextIndex > len(Books)-1:
        return nextIndex - len(Books)
    else:
        return nextIndex
    

async def list_files_async(directory):
    async for entry in os.listdir(directory):
        if entry.is_file():
            print(f"File: {entry.name}")
        elif entry.is_dir():
            print(f"Directory: {entry.name}")

def listDir(dir):
    Books.clear()
    fileNames = os.listdir(dir)
    index = 0
    for fileName in fileNames:
        folderPath = os.path.abspath(os.path.join(dir, fileName))
        if not os.path.isdir(folderPath):
            continue
        subDirs = os.listdir(os.path.abspath(os.path.join(dir, fileName)))
        listSubDir(subDirs, folderPath, index)
        # Copy images or the reference is kept and the previous book will have its images cleared when AllImages is cleared
        images = copy.deepcopy(AllImages)
        fileList = copy.deepcopy(FileList)
        newBook = (Book(index, fileName, images, Launcher['launchPath'], Launcher['launcherName'], "", fileList, "", False))
        Books.append(newBook)
        resetLauncher()
        index += 1

def listSubDir(subDirs, folderPath, index):
    for subName in subDirs:
        subFilePath = os.path.join(folderPath, subName)
        getFiles(subFilePath, subName)
        if os.path.isdir(subFilePath):
            subFileNames = os.listdir(subFilePath)
            for subFileName in subFileNames:
                subFolderPath = os.path.abspath(os.path.join(subFilePath, subFileName))
                getFiles(subFolderPath, subFileName)
                if not os.path.isdir(subFolderPath):
                    continue
                subDirs = os.listdir(subFolderPath)
                listSubDir(subDirs, subFolderPath, index)

def getFiles(subFilePath, subName):
    if not os.path.isfile(subFilePath):
        return

    # if file extension is missing continue
    if len(subName.split('.')) <= 1:
        return
    subFileExtension = subName.split('.')[1]
    if validImage(subFileExtension):
        AllImages.append(subFilePath)
    if validExecutable(subFileExtension):
        setLauncher(subFilePath, subName, subName)

def validImage(value):
    return value == 'png' or value == 'jpg' or value == 'gif'

def validExecutable(value):
    return  value == 'exe' or value == 'lnk'

def resetLauncher():
    Launcher['launchPath'] = ""
    Launcher['launcherName'] = ""
    AllImages.clear()
    FileList.clear()

def setLauncher(subLaunchPath, subLauncherName, subFileName):
    launchPath = Launcher['launchPath']
    launcherName = Launcher['launcherName']
    FileList[subLauncherName] = subLaunchPath
    if not launchPath and not launcherName:
        if subLaunchPath:
            # print(f'sub dir {subFileName} has outputted a new path {subLaunchPath}')
            Launcher['launchPath'] = subLaunchPath
        if subLauncherName:
            # print(f'sub dir {subFileName} has outputted a new name {subLauncherName}')
            Launcher['launcherName'] = subLauncherName
    # else:
    #     if subLaunchPath:
    #         print(f'sub dir {subFileName} has outputted a path {subLaunchPath} but it has already been set')
    #     if subLauncherName:
    #         print(f'sub dir {subFileName} has outputted a name {subLauncherName} but it has already been set')

def init():
    if LOADFROMSAVE:
        loadData()
    else:
        listDir(FOLDER_PATH)
    #TODO if there is save data load it and use it instead of listDir. listDir will reset all changes (ie covers)

def loadData():
    Books.clear()
    loadedData = fileService.loadData()
    #print(f'loadedData {loadedData}')
    #loadedData.keys
    for data in loadedData:
        #print(f'newBook {data.identifier} : {data.cover}')
        # for img in data.images:
        #     print(f'newBook {data.identifier} : {img}')
        Books.append(data)
        #print(f'Append newBook{data.identifier} via load with {len(data.images)} images')

def loadFromFolders(callback):
    listDir(FOLDER_PATH)
    callback()
    # try:
    #     await list_files_async(FOLDER_PATH)
    #     callback()
    # except Exception as e:
    #     print(f"Error in loadFromFolders: {e}")

def loadFromData(callback):
    loadData()
    callback()