import pygame
import os
import books
import fileService
from settings import *
from math import floor
from pygame.math import Vector2

class Portal(pygame.sprite.Sprite):
    def __init__(self, identifier):
        super().__init__()
        self.identifier = identifier
        self.width = WIDTH/4
        self.height = HEIGHT/4
        self.book = None
        self.isHovered = False
        self.pos = Vector2()
        self.target_pos = Vector2()

        # Test: Displays portal identifier in the portal
        if TEST :
            ipag_font = pygame.font.Font('font/ipag.ttf', 68)
            self.index_number = ipag_font.render(f'{self.identifier}', False, 'Black')
            self.index_rect = self.index_number.get_rect(midbottom = (100,100))
            bid = self.book.identifier if self.book != None else "N/A" 
            self.book_index = ipag_font.render(f'Book {bid}', False, 'Black')
            self.book_index_rect = self.index_number.get_rect(midbottom = (200,100))
        # ----

        # Positioning
        if HORIZONTALUI:
            self.spacing_x = WIDTH/3
            self.pos.x = (identifier - 2) * self.spacing_x + WIDTH/2
            self.pos.y = 2 * HEIGHT/5
        else:
            self.spacing_y = self.height + 75
            modulum_x = identifier % 4
            modulum_y = floor(identifier / 4)
            self.pos.x = self.width/2 + self.width * modulum_x
            self.pos.y = self.height/2 + self.spacing_y * modulum_y

        self.setBG()
        self.rect = self.image.get_rect(center = self.pos)

        self.is_moving = False
        self.dir = 0

        if HORIZONTALUI:
            self.poolH()
        else:
            self.poolV()

    def getTitle(self):
        return 'NO TITLE' if self.book == None else self.book.title

    def getFileName(self):
        return 'NO FILE' if self.book == None else self.book.fileName
    
    def getAvailableCovers(self):
        cover_list = []
        for cover in self.book.covers:
            cover_list.append(os.path.splitext(os.path.basename(cover))[0])
        return ['N/A'] if self.book == None else cover_list

    def setBookCover(self, cover):
        self.book.setCover(cover)

    def setBook(self, index):
        # print(f'Portal {self.identifier} sets book {index}')
        self.book = next(b for b in books.Books if b.identifier == index)
        self.setCover()
        # self.imageRects = {}
        # self.setImages()
        if TEST :
            ipag_font = pygame.font.Font('font/ipag.ttf', 68)
            bid = self.book.identifier if self.book != None else "N/A" 
            self.book_index = ipag_font.render(f'Book {bid}', False, 'Black')
            self.book_index_rect = self.index_number.get_rect(midbottom = (200,100))
        # ----

    def setBG(self):
        self.bg_surface = pygame.Surface((self.width,self.height))
        self.bg_surface.fill(BG_COLOR)
        self.image = self.bg_surface

    def setCover(self):
        self.setBG() # TODO: Find out if setting the bg again is really needed
        if self.book.cover:
            self.cover = fileService.load_image(self.book.cover, False)
            self.cover = self.fitImage(self.cover)
            self.coverRect = self.cover.get_rect(center = (self.width/2,self.height/2))
    # TODO: image is a property of Sprite so I should probably seperate these screenshot images from that with a rename
    def setImages(self):
        if self.book.images:
            self.images = fileService.load_images(self.book.images, False)
            for image in self.images:
                image = self.fitImage(image)
                self.imageRects[image] = image.get_rect(center = (self.width/2,self.height/2))

    def fitImage(self, image):
        x,y = image.get_size()
        rx = self.width / x
        ry = self.height / y
        ratio = rx if rx < ry else ry
        image = pygame.transform.scale(image, (int(x*ratio),int(y*ratio)))
        return image

    def next(self, dir):
        if self.is_moving:
            return
        if HORIZONTALUI:
            self.target_pos.x = self.rect.x + self.spacing_x * dir
            self.target_pos.y = self.pos.y
            self.dir = dir
        else:
            self.target_pos.x = self.pos.x
            self.target_pos.y = self.rect.y + self.spacing_y * dir
            self.dir = dir
        self.is_moving = True

    def move(self):
        if not self.is_moving:
            return
        if HORIZONTALUI:
            #self.move_towards(self.target_pos, SPEED)
            self.moveHorizontal()
        else:
            self.moveVertical()

    #TODO: This method is for experimenting looking for a smoother movement
    def move_towards(self, target, speed):
        direction = target - self.pos
        distance = direction.length()
        if distance > 0:
            normalized_direction = direction.normalize()
            self.pos += normalized_direction * min(speed, distance)
            self.rect.center = round(self.pos.x), round(self.pos.y)

    def moveHorizontal(self):
        if (self.dir == -1 and self.rect.x <= self.target_pos.x) or self.dir == 1 and self.rect.x >= self.target_pos.x:
            self.dir = 0
            self.is_moving = False
            self.rect.x = self.target_pos.x
            #respawn from pool
            if self.rect.x == self.left_end_pos:
                self.rect.x = self.right_respawn_pos
                portal_index = self.identifier - 1 if self.identifier - 1 >= 0 else HORIZONTALPORTALNO - 1
                self.setNextBook(portal_index, 1)
            if self.rect.x == self.right_end_pos:
                self.rect.x = self.left_respawn_pos
                portal_index = self.identifier + 1 if self.identifier + 1 <= HORIZONTALPORTALNO - 1 else 0
                self.setNextBook(portal_index, -1)
            return
        self.rect.x += self.dir * SPEED

    def moveVertical(self):
        if (self.dir == -1 and self.rect.y <= self.target_pos.y) or self.dir == 1 and self.rect.y >= self.target_pos.y:
            # dir -1 Add portals at the end 
            self.dir = 0
            self.is_moving = False
            self.rect.y = self.target_pos.y
            #respawn from pool
            firstLine = 0
            lastLine = 0 + (self.spacing_y * 3)
            if self.target_pos.y <= firstLine - (self.spacing_y):
                self.rect.y = lastLine
                self.setNextBook(self.identifier, VERTICALPORTALNO)
            if self.target_pos.y > lastLine:
                self.rect.y = firstLine
                self.setNextBook(self.identifier, -VERTICALPORTALNO)
            return
        self.rect.y += self.dir * SPEED
    
    def setNextBook(self, currentIndex, mod):
        index = Portals.sprites()[currentIndex].book.identifier + mod
        book_index = books.getNextBook(index)
        self.setBook(book_index)

    # This despawns repositions the offscreen portal and loads the next book in line
    # TODO: if there are less than 3 portals this breaks. Probably breaks in other places too then...
    def poolH(self):
        self.target_pos.x = self.rect.x + self.spacing_x * self.dir
        despawn_pos = HORIZONTALPORTALNO - 2 # TODO: mod is 1 for 3 portals 2 for 5 and so on. Do not hard code it.  HORIZONTALPORTALNO - mod
        respawn_pos = HORIZONTALPORTALNO - 3 # TODO: mod is 2 for 3 portals 3 for 5 and so on. Do not hard code it.  HORIZONTALPORTALNO - mod
        self.left_end_pos = WIDTH/2 - (despawn_pos * self.spacing_x) - self.width/2
        self.right_end_pos = WIDTH/2 + (despawn_pos * self.spacing_x) - self.width/2
        self.left_respawn_pos = WIDTH/2 - (respawn_pos * self.spacing_x) - self.width/2
        self.right_respawn_pos = WIDTH/2 + (respawn_pos * self.spacing_x) - self.width/2

    def poolV(self):
        self.target_pos.y = self.rect.y + self.spacing_y * self.dir
        return

    def onHover(self):
        mouse_pos = pygame.mouse.get_pos()
        self.isHovered = self.rect.collidepoint(mouse_pos)
        return self.isHovered

    def onClick(self):
        mouse_pos = pygame.mouse.get_pos()
        if self.rect.collidepoint(mouse_pos):
            self.launch()

    def launch(self):
        if self.book == None:
            print(f'Index {self.identifier} has no book')
            return
        self.book.Launch()

    def draw(self):
        # self.cover isn't defined if missing so I'm checking for the book cover. 
        if self.book != None and self.book.cover:
            self.bg_surface.blit(self.cover, self.coverRect)
        # for image in self.imageRects:
        #     self.bg_surface.blit(image, self.imageRects[image])
        
        # test
        if TEST :
            self.bg_surface.blit(self.index_number, self.index_rect)
            self.bg_surface.blit(self.book_index, self.book_index_rect)

    def update(self):
        self.move()
        self.draw()

ActiveIndex = 2
Portals = pygame.sprite.Group()

def ActiveTitle():
    return f'{Portals.sprites()[ActiveIndex].getTitle()}'

def ActiveLauncher():
    return f'{Portals.sprites()[ActiveIndex].getFileName()}'

def ActiveAvailableCovers():
    return Portals.sprites()[ActiveIndex].getAvailableCovers()

def ActiveLaunch():
    print(f'Launch Active : {ActiveTitle()}')
    Portals.sprites()[ActiveIndex].launch()

def SpawnPortal(id):
    new_portal = Portal(id)
    Portals.add(new_portal)

def SpawnPortals(fromId, number):
    for p in range(number):
        SpawnPortal(p + fromId)

def SetBook(portalIndex, bookIndex):
    Portals.sprites()[portalIndex].setBook(bookIndex)

def SetCover(cover):
    Portals.sprites()[ActiveIndex].setBookCover(cover)

def load_portals():
    print(f'Books: {len(books.Books)}')

    # The first book will be in portal index 2 (The center portal) if Horizontal
    if HORIZONTALUI:
        firstBookIndex = len(books.Books) - 2
    else:
        firstBookIndex = 0

    bookIndex = firstBookIndex

    noOfPortals = HORIZONTALPORTALNO if HORIZONTALUI else VERTICALPORTALNO
    for p in range(noOfPortals):
        new_portal = Portal(p)
        new_portal.setBook(bookIndex)
        Portals.add(new_portal)
        bookIndex = books.getNextBook(bookIndex+1)

    return Portals
