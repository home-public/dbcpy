import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from settings import *
from bookTab import bookTab
from settingsTab import settingsTab

class hub:
    def __init__(self, master1):
        master1.title("Bookshelf editor")
        self.panel1 = tk.Frame(master1)
        self.panel1.grid()
        master1.geometry(RESOLUTION)

        def aboutCommand():
            messagebox.showinfo(
                title='About', message="An Underdeveloped soft program")
                
        #Create Menu
        self.my_menu = tk.Menu(master1)
        master1.option_add('*tearOff', False)
        master1.config(menu=self.my_menu)

        self.file_menu = tk.Menu(self.my_menu)
        self.my_menu.add_cascade(label="File", menu = self.file_menu)
        self.file_menu.add_command(label="Exit", command=master1.quit)

        self.edit_menu = tk.Menu(self.my_menu)
        self.my_menu.add_cascade(label="Edit", menu = self.edit_menu)

        self.help_menu = tk.Menu(self.my_menu)
        self.my_menu.add_cascade(label="Help", menu = self.help_menu)
        self.help_menu.add_command(label="About", command=aboutCommand)

        # Create Notebook
        self.my_notebook = ttk.Notebook(root1)
        self.tabs = []
        self.windows = []

        # Add Tab
        self.tabs.append(tk.Frame(self.my_notebook, width=300, height=500))
        self.windows.append(bookTab(self.tabs[0]))

        self.tabs.append(tk.Frame(self.my_notebook, width=300, height=500))
        self.windows.append(settingsTab(self.tabs[1]))

        # Draw Notebook
        self.my_notebook.grid(row=0, column=0)

        # Draw Tab
        self.tabs[0].pack(fill="both", expand=1)
        self.my_notebook.add(self.tabs[0], text="Book")

        self.tabs[1].pack(fill="both", expand=1)
        self.my_notebook.add(self.tabs[1], text="Settings")

root1 = tk.Tk()

editorHub = hub(root1)

root1.mainloop()