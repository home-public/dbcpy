import tkinter as tk
import books
import asyncio

class MyApplication:
    def __init__(self):
        self.root = tk.Tk()
        self.root.title("My App")
        self.button_enabled = True
        self.lock = False

        #self.buttonLoad = tk.Button(self.root, text="LOAD", command=self.load_books)
        self.buttonLoad = tk.Button(self.root, text="LOAD", command=self.load_books_wrapper)
        self.buttonLoad.grid(row=0, column=0)

    # Create a wrapper function that awaits the coroutine
    def load_books_wrapper(self):
        print(f'load_books_wrapper')
        self.root.event_generate("<<LoadBooks>>")  # Trigger a custom event
        self.root.wait_variable(self.load_books_done)  # Wait for the coroutine to finish

    # Trigger the load_books coroutine when the custom event is generated
        self.root.bind("<<LoadBooks>>", lambda event: asyncio.create_task(self.load_books()))

    async def load_books(self):
        if not self.button_enabled or self.lock:
            print("Load Books Prevented")
            return

        self.button_enabled = False
        self.buttonLoad.config(state=tk.DISABLED)

        self.lock = True

        try:
            await books.loadFromFolders(self.callback_func)
        except Exception as e:
            print(f"Error in loadFromFolders: {e}")

        # Re-enable the button after execution
        self.button_enabled = True
        self.buttonLoad.config(state=tk.NORMAL)

        self.lock = False

    def load_books_done(self):
        print(f'load_books_done')

    def callback_func(self):
        print(f'Book Tab Loaded {len(books.Books)} books')

    def run(self):
        self.root.mainloop()

if __name__ == "__main__":
    app = MyApplication()
    app.run()