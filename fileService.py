import pygame
import io
import jsonpickle
import books

def load_sound():
    bgm = pygame.mixer.Sound('audio/music.wav')
    bgm.set_volume(.1)
    #bgm.play(loops = -1) # -1 = Endless loop
    return bgm

def load_sfx():
    sfx = pygame.mixer.Sound('audio/jump.mp3')
    sfx.set_volume(.1)
    return sfx

def load_image(image, convertAlpha):
    # print(f'Convert : {image}')
    if convertAlpha:
        return pygame.image.load(image).convert_alpha()
    else:
        return pygame.image.load(image).convert()

def load_images(images, convertAlpha):
    surfaces = []
    for image in images:
        # print(f'Convert : {image}')
        if convertAlpha:
            surfaces.append(pygame.image.load(image).convert_alpha())
        else:
            surfaces.append(pygame.image.load(image).convert())
    return surfaces

def fitImage(width, height, image):
    x,y = image.get_size()
    rx = width / x
    ry = height / y
    ratio = rx if rx < ry else ry
    image = pygame.transform.scale(image, (int(x*ratio),int(y*ratio)))
    return image

# This fits the text given to the width specified in pixels.
def fit_text_to_width(text, color, pixels, font_face = None):
    font = pygame.font.SysFont(font_face, pixels *3 // len(text) ) 
    text_surface = font.render(text, True, color)
    size = text_surface.get_size()
    size = ( pixels, int(size[1] * pixels / size[0]) )
    return pygame.transform.scale(text_surface, size)

def fit_text_to_width_from_surface(text_surface, target_size):
    x,y = text_surface.get_size()
    width,height = target_size
    rx = width / x
    ry = height / y
    ratio = rx if rx < ry else ry
    return pygame.transform.scale(text_surface, (int(x*ratio),int(y*ratio)))

def saveData(data):
    newJson = jsonpickle.encode(data) #, unpicklable=False, make_refs=False)
    setJson(newJson)
    #print(f'saveData newJson: {newJson}')

def loadData():
    j = getJson('saveData.json')
    #print(f'getJson : {j}')
    #return json.loads(j)
    return jsonpickle.decode(j)

def getJson(path):
    with io.open(path, 'r', encoding='utf-8') as f:
        json_string = f.read()
    return json_string
    
def setJson(data):
    with io.open('saveData.json', 'w', encoding='utf-8') as f:
        f.write(data)