import tkinter as tk

class settingsTab:
    def __init__(self, master1):
        self.panel1 = tk.Frame(master1)
        self.panel1.grid()

        #Gather tokens. The +1 is for the ID
        def createText():
            print("On Save Settings")

        #Add selectable non argument tokens select box
        self.frame = tk.LabelFrame(self.panel1, text="Tokens", padx=5, pady=5)

        #Add Save button
        self.buttonAdd = tk.Button(self.panel1, text="Save", command=createText)

        #Place in the grid
        self.startIndex = 1
        self.frame.grid(row=self.startIndex, column=0, columnspan=2, padx=10, pady=10)

        self.buttonAdd.grid(row=self.startIndex+1, column=0)