import pygame
from settings import *

class OptionBox():

    def __init__(self, x, y, w, h, font, option_list, selected = 0):
        self.color = COVERDROPNEUTRALCOLOR 
        self.highlight_color = COVERDROPHIGHLIGHTCOLOR
        self.rect = pygame.Rect(x, y, w, h)
        self.font = font
        self.option_list = option_list
        self.selected = selected
        self.draw_menu = False
        self.menu_active = False
        self.active_option = -1
        self.bg_surface = pygame.display.set_mode((WIDTH,HEIGHT))
        self.bg_surface.fill((0, 0, 0))
        self.mask = pygame.mask.from_surface (self.bg_surface)

    def overUI(self, mouse_pos):
        if self.bg_surface.collidepoint (mouse_pos):
            offset_x = mouse_pos [0] - self.rect.x
            offset_y = mouse_pos [1] - self.rect.y
            value = self.mask.get_at ((offset_x, offset_y))
            return value
        else:
            return False
        
    # Currently options are required. TODO: Guard against empty list.
    def setOptions(self, option_list):
        self.selected = -1
        self.option_list = option_list

    def draw(self):
        # fail safe
        if self.selected > len(self.option_list):
            self.selected = -1

        pygame.draw.rect(self.bg_surface, self.highlight_color if self.menu_active else self.color, self.rect)
        pygame.draw.rect(self.bg_surface, (0, 0, 0), self.rect, 2)
        msg = self.font.render(self.option_list[self.selected], 1, (0, 0, 0))
        self.bg_surface.blit(msg, msg.get_rect(center = self.rect.center))

        if self.draw_menu:
            for i, text in enumerate(self.option_list):
                rect = self.rect.copy()
                rect.y += (i+1) * self.rect.height
                pygame.draw.rect(self.bg_surface, self.highlight_color if i == self.active_option else self.color, rect)
                msg = self.font.render(text, 1, (0, 0, 0))
                self.bg_surface.blit(msg, msg.get_rect(center = rect.center))
            outer_rect = (self.rect.x, self.rect.y + self.rect.height, self.rect.width, self.rect.height * len(self.option_list))
            pygame.draw.rect(self.bg_surface, (0, 0, 0), outer_rect, 2)

    def update(self):
        mpos = pygame.mouse.get_pos()
        self.menu_active = self.rect.collidepoint(mpos)
        
        self.active_option = -1
        for i in range(len(self.option_list)):
            rect = self.rect.copy()
            rect.y += (i+1) * self.rect.height
            if rect.collidepoint(mpos):
                self.active_option = i
                break

        if not self.menu_active and self.active_option == -1:
            self.draw_menu = False
    
    def onClick(self):
        if self.menu_active:
            self.draw_menu = not self.draw_menu
        elif self.draw_menu and self.active_option >= 0:
            self.selected = self.active_option
            self.draw_menu = False
            return self.option_list[self.active_option]
        return None

    # def draw(self, surf):
    #     pygame.draw.rect(surf, self.highlight_color if self.menu_active else self.color, self.rect)
    #     pygame.draw.rect(surf, (0, 0, 0), self.rect, 2)
    #     msg = self.font.render(self.option_list[self.selected], 1, (0, 0, 0))
    #     surf.blit(msg, msg.get_rect(center = self.rect.center))

    #     if self.draw_menu:
    #         for i, text in enumerate(self.option_list):
    #             rect = self.rect.copy()
    #             rect.y += (i+1) * self.rect.height
    #             pygame.draw.rect(surf, self.highlight_color if i == self.active_option else self.color, rect)
    #             msg = self.font.render(text, 1, (0, 0, 0))
    #             surf.blit(msg, msg.get_rect(center = rect.center))
    #         outer_rect = (self.rect.x, self.rect.y + self.rect.height, self.rect.width, self.rect.height * len(self.option_list))
    #         pygame.draw.rect(surf, (0, 0, 0), outer_rect, 2)

    # def update(self, event_list):
    #     mpos = pygame.mouse.get_pos()
    #     self.menu_active = self.rect.collidepoint(mpos)
        
    #     self.active_option = -1
    #     for i in range(len(self.option_list)):
    #         rect = self.rect.copy()
    #         rect.y += (i+1) * self.rect.height
    #         if rect.collidepoint(mpos):
    #             self.active_option = i
    #             break

    #     if not self.menu_active and self.active_option == -1:
    #         self.draw_menu = False
    #     for event in event_list:
    #         if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
    #             if self.menu_active:
    #                 self.draw_menu = not self.draw_menu
    #             elif self.draw_menu and self.active_option >= 0:
    #                 self.selected = self.active_option
    #                 self.draw_menu = False
    #                 return self.active_option
    #     return -1




# pygame.init()
# clock = pygame.time.Clock()
# window = pygame.display.set_mode((640, 480))

# list1 = OptionBox(
#     40, 40, 160, 40, (150, 150, 150), (100, 200, 255), pygame.font.SysFont(None, 30), 
#     ["option 1", "2nd option", "another option"])

# run = True
# while run:
#     clock.tick(60)
#     event_list = pygame.event.get()
#     for event in event_list:
#         if event.type == pygame.QUIT:
#             run = False

#     selected_option = list1.update(event_list)
#     if selected_option >= 0:
#         print(selected_option)

#     window.fill((255, 255, 255))
#     list1.draw(window)
#     pygame.display.flip()
    
# pygame.quit()
# exit()